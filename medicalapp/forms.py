from django import forms
from .models import *



class DateTimeInput(forms.DateInput):
    input_type = 'time'


class AdminLoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}))



class AdminOrganizationCreateForm(forms.ModelForm):
    class Meta:
        model = Organization
        exclude = ['created_at', 'updated_at', 'deleted_at']

    def __init__(self, *args, **kwargs):
        super(AdminOrganizationCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminGalleryCreateForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminGalleryCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminGalleryDeleteForm(forms.ModelForm):

    class Meta:
        model = Gallery
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminNoticeCreateForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = ['title', 'date', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminNoticeCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminNoticeDeleteForm(forms.ModelForm):

    class Meta:
        model = Notice
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminStaffCreateForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = ['name', 'image', 'post', 'phone1', 'email', 'subject']
        
    def __init__(self, *args, **kwargs):
        super(AdminStaffCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminStaffDeleteForm(forms.ModelForm):

    class Meta:
        model = Staff
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminTestimonialCreateForm(forms.ModelForm):
    class Meta:
        model = Testimonial
        fields = ['name', 'post', 'says', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminTestimonialCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminTestimonialDeleteForm(forms.ModelForm):

    class Meta:
        model = Testimonial
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminSliderCreateForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = ['image', 'caption']

    def __init__(self, *args, **kwargs):
        super(AdminSliderCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminSliderDeleteForm(forms.ModelForm):

    class Meta:
        model = Slider
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class MessageForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Name.'
            }))
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Phone Number.'
            }))
    email = forms.CharField(
        widget=forms.EmailInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Email.'
            }))
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'class': 'form-control', 'placeholder': 'Write Your message.'
            }))
    class Meta:
        model = Message
        fields = ['name', 'phone', 'email', 'message']

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminMessageDeleteForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminDepartmentCreateForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = ['image', 'caption', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminDepartmentCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminDepartmentDeleteForm(forms.ModelForm):

    class Meta:
        model = Department
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }