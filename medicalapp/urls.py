from django.conf.urls import url 
from .views import *


urlpatterns = [

    url(r'^hospital-admin/logout/$', AdminLogout.as_view(),
        name='adminlogout'),
    url(r'^hospital-admin/login/$', AdminLogin.as_view(),
        name='adminlogin'),



    url(r'^hospital-admin/$', AdminHomeView.as_view(), name='adminhome'),
    url(r'^hospital-admin/hospital/create/$',
        AdminOrganizationCreateView.as_view(), name='adminorganizationcreate'),
    url(r'^hospital-admin/hospital/(?P<pk>\d+)/update/$',
        AdminOrganizationUpdateView.as_view(), name='adminorganizationupdate'),





    # client
    url(r'^$', ClientHomeView.as_view(), name='clienthome'),
	url(r'^about/$', ClientAboutView.as_view(), name='clientabout'),
	url(r'^gallery/$', ClientGalleryListView.as_view(), name='clientgallerylist'),
	url(r'^blog/$', ClientDepartmentListView.as_view(), name='clientdepartmentlist'),
    url(r'^contact/$', ClientContactView.as_view(), name='clientcontact'),



    ]