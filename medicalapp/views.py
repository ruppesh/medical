from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *


class AdminLogout(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class AdminLogin(FormView):
    template_name = "medicalapp/admintemplates/adminlogin.html"
    form_class = AdminLoginForm
    success_url = '/school-admin/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': 'Incorrect username or password'})
        return super().form_valid(form)





class AdminMixin(LoginRequiredMixin, object):
    login_url = '/school-admin/login/'

    def get_context_data(self, *args, **kwargs):
        context = super(AdminMixin, self).get_context_data(*args, **kwargs)
        context['organization'] = Organization.objects.get(id=1)
        return context


class DeleteMixin(LoginRequiredMixin, UpdateView):
    login_url = '/school-admin/login/'

    def form_valid(self, form):
        form.instance.deleted_at = timezone.now()
        form.save()
        return super().form_valid(form)


class AdminHomeView(AdminMixin, TemplateView):
    template_name = 'medicalapp/admintemplates/adminhome.html'

class AdminOrganizationCreateView(AdminMixin, CreateView):
    template_name = 'medicalapp/admintemplates/adminorganizationcreate.html'
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('medicalapp:adminhome')


class AdminOrganizationUpdateView(AdminMixin, UpdateView):
    template_name = 'medicalapp/admintemplates/adminorganizationcreate.html'
    model = Organization
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('medicalapp:adminhome')




# client
class ClientMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(ClientMixin, self).get_context_data(*args, **kwargs)
        context['sliders'] = Slider.objects.filter(deleted_at__isnull=True)
        context['notices'] = Notice.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['testimonials'] = Testimonial.objects.filter(
            deleted_at__isnull=True).order_by('-id')
        context['organization'] = Organization.objects.get(id=1)

        return context



class ClientHomeView(ClientMixin, TemplateView):
	template_name='medicalapp/clienttemplates/clienthome.html'


class ClientAboutView(ClientMixin, ListView):
    template_name='medicalapp/clienttemplates/clientabout.html'
    queryset = Staff.objects.filter(deleted_at__isnull=True)
    context_object_name = 'staffs'



class ClientGalleryListView(ClientMixin, ListView):
    template_name='medicalapp/clienttemplates/clientgallerylist.html'
    queryset = Gallery.objects.filter(deleted_at__isnull=True)
    context_object_name ='gallerys'


class ClientContactView(ClientMixin, CreateView):
    template_name='medicalapp/clienttemplates/clientcontact.html'
    form_class = MessageForm
    success_url = '/'


class ClientDepartmentListView(ClientMixin, ListView):
    template_name='medicalapp/clienttemplates/clientdepartmentlist.html'
    queryset = Staff.objects.filter(deleted_at__isnull=True)
    context_object_name = 'staffs'