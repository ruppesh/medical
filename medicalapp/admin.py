from django.contrib import admin
from .models import *


admin.site.register([Notice, Staff, 
	Gallery, Organization, Testimonial, Slider, Message, Department ])
